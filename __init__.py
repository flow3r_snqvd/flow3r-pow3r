from st3m import logging
from st3m.application import Application, ApplicationContext
from st3m.input import InputController, InputState
from ctx import Context
import st3m.run

import bl00mbox
import math

class Track():
    def __init__(self, name, sample, steps, base_color, highlight_color, ring_size):
        self.name = name
        self.steps = steps
        self.base_color = base_color
        self.highlight_color = highlight_color

        self.ring_size = ring_size
        self.pos = 0
        
        self.is_selected = False

        self.selected_bars = [False] * steps

        self.bpm = 80
        self.interval = 60 * 1000 / ( self.bpm * 4)
        self.trac_delta_ms = 0

        self.sample = sample

        # draw constants
        self.rotate =  2 * math.pi / self.steps
        self.arc_angle2 = 2 * math.pi / (self.steps + 1) 
        self.arc_angle2_full = 2 * math.pi

    def step(self) -> None:
        if self.pos < self.steps - 1:
            self.pos += 1
        else:
            self.pos = 0
        if self.selected_bars[self.pos]:
            # print(f"[{self.name}] play sample for pos: {self.pos}")
            self.sample.signals.trigger.start()
        # print(f"{self.name} pos: {self.pos}")

    def set_bars(self, petals: list(any)) -> None:
        if petals[2].pressed:
            self.selected_bars[self.pos] = True
        if petals[4].pressed:
            self.selected_bars[self.pos] = False

    def update(self, delta_ms: int, petals: list(any)) -> None:
        self.trac_delta_ms += delta_ms 

        if self.trac_delta_ms > self.interval:
            self.step()
            self.trac_delta_ms = self.trac_delta_ms - self.interval
        
        if self.is_selected:
            self.set_bars(petals)

    def select(self, value: bool) -> None:
        self.is_selected = value

    def draw(self, ctx: Context) -> None:
        ctx.line_width = 10
        
        for i in range(self.steps):
            color = self.base_color

            if self.selected_bars[i]:
                color = self.highlight_color
            
            if i == self.pos: 
                color = self.highlight_color

            ctx.rgb(
                color[0],color[1],color[2]
                ).arc(
                0, 0, self.ring_size, 0, self.arc_angle2 , 0
                ).rotate(self.rotate).stroke()
        if self.is_selected:
            ctx.line_width = 16
            ctx.rgba(
                255,255,255,0.4
                ).arc(
                0, 0, self.ring_size, 0,self.arc_angle2_full, 0
                ).stroke()

class Flow3rPow3r(Application):
    log = logging.Log(__name__, level=logging.INFO)

    channel: bl00mbox.Channel
    sample_names: list[str] = [
        "kick.wav",
        "snare.wav",
        "hihat.wav",
        "crash.wav",
        "open.wav",
        "close.wav"
    ]

    track_parameter: list[dict] = [
        {"name": "kick", "steps": 24 , "base_color": (0,0,255) , "highlight_color": (0,255,0), "ring_size": 115},
        {"name": "snare", "steps": 16 , "base_color": (0,255,0) , "highlight_color": (255,0,0), "ring_size": 100},
        {"name": "hihat", "steps": 10 , "base_color": (255,0,0) , "highlight_color": (0,0,255), "ring_size": 85},
        {"name": "crash", "steps": 16 , "base_color": (0,0,255) , "highlight_color": (0,255,0), "ring_size": 70},
        {"name": "open", "steps": 9 , "base_color": (0,255,0) , "highlight_color": (255,0,0), "ring_size": 55},
        {"name": "close", "steps": 12 , "base_color": (0,0,255) , "highlight_color": (0,0,255), "ring_size": 40},
    ]
    
    is_loading: bool = True

    def __init__(self, app_ctx: ApplicationContext):
        super().__init__(app_ctx)

        self.input = InputController()

        self.tracks = []
        self.channel = bl00mbox.Channel("Simple Drums")

        self.track_selected = 0;
        self.tracks_created = 5;

    def draw(self, ctx: Context) -> None:
        ctx.rgb(0, 0, 0).rectangle(-120, -120, 240, 240).fill()

        if self.is_loading:
            if len(self.tracks) != len(self.sample_names):
                ctx.move_to(0, -10)
                ctx.text_align = ctx.CENTER
                ctx.text_baseline = ctx.MIDDLE
                ctx.font_size = 20
                ctx.font = "Camp Font 1"
                ctx.rgb(0, 255, 255).text(f"Loading sample {len(self.tracks) + 1}/{len(self.sample_names)}")
            ctx.restore()
            return
        else:
            for track in self.tracks:
                track.draw(ctx)

    def think(self, ins: InputState, delta_ms: int) -> None:
        self.input.think(ins, delta_ms)

        if self.is_loading:
            self._load_next_sample()
            return


        current_track_selected = self.track_selected
        if self.input.buttons.app.left.pressed:
            if self.track_selected > 0:
                self.track_selected -= 1
            else:
                self.track_selected = self.tracks_created - 1

        if self.input.buttons.app.right.pressed:
            if self.track_selected + 1 < self.tracks_created:
                self.track_selected += 1
            else:
                self.track_selected = 0

        if current_track_selected != self.track_selected:
            for track in self.tracks:
                track.select(False)
            self.tracks[self.track_selected].select(True)

        for track in self.tracks:
            track.update(delta_ms, ins.captouch.petals)
    
    def _load_next_sample(self) -> None:
        loaded = len(self.tracks)
        if len(self.tracks) >= self.tracks_created:
            self.tracks[0].select(True)
            # self.tracks[self.track_selected].select(True)
            self.is_loading = False
            return

        i = len(self.tracks)

        sample = self.channel.new(bl00mbox.patches.sampler, self.sample_names[i])
        sample.signals.output = self.channel.mixer

        params = self.track_parameter[i]
        self.tracks.append(Track(params["name"], sample, params["steps"], params["base_color"], params["highlight_color"], params["ring_size"]))

if __name__ == '__main__':
    st3m.run.run_view(Flow3rPow3r(ApplicationContext()))
